module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
		
		concat: {
			banner: "'use strict';\n",
			js: {
				src: ['src/app.js', 'src/controllers.js'],
				dest: 'js/app.js'
			}
		},
		
		uglify: {
			dist: {
				src: ['<%= concat.js.dest %>'],
				dest: 'js/app.min.js'
			}
		},
		
		sass: {
			dev: {
				options: {
					style: 'expanded'
				},
				files: {
					'css/style.css': 'sass/main.sass'
				}
			}
		},
		
		imagemin: {
			dynamic: {
				options: {
					optimizationLevel: 5,
					progressive: true
				},
				files: [{
					expand: true,
					cwd: 'img',
					src: ['*'],
					dest: 'i'
				}],
			}
		},
		
		watch: {
			options: {
				livereload: true,
			},
			scripts: {
				files: ['src/*.js'],
				tasks: ['newer:concat', 'newer:uglify'],
				options: {
					spawn: false
				}
			},
			css: {
				files: ['sass/*.sass'],
				tasks: ['newer:sass'],
				options: {
					spawn: false
				}
	/*
			},
			imageopti: {
        files: ['img/*.*'],
        tasks: ['newer:imagemin']
	*/
      }
		},
		
		ftp_push: {
			demo: {
				options: {
					authKey: 'netology',
					host: 'university.netology.ru',
					dest: '/fbb/',
					port: 21
				},
				files: [{
					expand: true,
					cwd: '.',
					src: [
								'index.html',
								'css/main.css'
					]
					}]
			}
		}
		
  });

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-ftp-push');
	
  grunt.registerTask('default', ['concat', 'uglify', 'sass', 'watch']);

};