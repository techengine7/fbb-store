var fbbBooksApp = angular.module('fbbBooksApp', ['ngRoute']);
fbbBooksApp.config(['$locationProvider', '$routeProvider', function($local, $routes) {
  //$local.hashPrefix('!');
	$routes
	.when('/',{
		templateUrl: 'tpl/home.html',
		controller: 'BooksListCtrl'
	})
	.when('/about',{
		templateUrl: 'tpl/about.html',
		controller: 'AboutCtrl'
	})
	.when('/contact',{
		templateUrl: 'tpl/contact.html',
		controller: 'ContactCtrl'
	})
	.when('/book/:bookId',{
		templateUrl: 'tpl/book.html',
		controller: 'BookCtrl'
	})
	.otherwise({
		redirectTo: '/'
	});
}]);

fbbBooksApp.controller('BooksListCtrl',['$scope','$http', '$location', function($scope, $http, $location) {
	/*
		console.log($location.url());
		console.log($location.path());
		console.log($location.search());
		console.log($location.hash());
	*/
    $http.get('https://netology-fbb-store-api.herokuapp.com/book/').success(function(data, status, headers, config) {
        $scope.books = data;
				$scope.header = 'Мы издаем книги — вы их можете не читать';
    });
}]);

fbbBooksApp.controller('AboutCtrl',['$scope','$http', '$location', function($scope, $http, $location) {

}]);

fbbBooksApp.controller('ContactCtrl',['$scope','$http', '$location', function($scope, $http, $location) {

}]);

fbbBooksApp.controller('BookCtrl',['$scope','$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
	$scope.bookId = $routeParams.bookId;
	$http.get('https://netology-fbb-store-api.herokuapp.com/book/'+$scope.bookId).success(function(data, status, headers, config) {
    $scope.singleBook = data;
  });
}]);